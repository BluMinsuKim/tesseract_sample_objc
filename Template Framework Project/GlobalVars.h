//
//  GlobalVars.h
//  Template Framework Project
//
//  Created by korea on 2016. 12. 7..
//  Copyright © 2016년 Daniele Galiotto - www.g8production.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVars : NSObject
{
    NSArray *imageList;
    BOOL emptyArray;
    NSData *cropImage;
    NSString *imageName;
}

+ (GlobalVars *)sharedInstance;

@property (strong, nonatomic, readwrite) NSArray *imageList;
@property (nonatomic, readwrite) BOOL emptyArray;
@property (strong, nonatomic, readwrite) NSData *cropImage;
@property (strong, nonatomic, readwrite) NSString *fileName;

@end
