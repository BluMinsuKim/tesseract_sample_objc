//
//  GlobalVars.m
//  Template Framework Project
//
//  Created by korea on 2016. 12. 7..
//  Copyright © 2016년 Daniele Galiotto - www.g8production.com. All rights reserved.
//

#import "GlobalVars.h"

@implementation GlobalVars

@synthesize imageList;
@synthesize emptyArray;
@synthesize cropImage;
@synthesize fileName;

+ (GlobalVars *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalVars *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalVars alloc]init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        imageList = [[NSArray alloc]init];
        emptyArray = YES;
        cropImage = [[NSData alloc]init];
        fileName = [[NSString alloc]init];
    }
    return self;
}

@end
