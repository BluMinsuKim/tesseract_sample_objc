//
//  ImageCropViewController.h
//  Template Framework Project
//
//  Created by korea on 2016. 12. 7..
//  Copyright © 2016년 Daniele Galiotto - www.g8production.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CropperView.h"
#import "PhotoTweakView.h"
#import "PhotoTweaksViewController.h"

@interface ImageCropViewController : UIViewController

@property (strong,nonatomic) NSData *receiveImage;
@property (strong, nonatomic) IBOutlet CropperView *cropperSourceView;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;

@end
