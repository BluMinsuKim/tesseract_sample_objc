//
//  ImageCropViewController.m
//  Template Framework Project
//
//  Created by korea on 2016. 12. 7..
//  Copyright © 2016년 Daniele Galiotto - www.g8production.com. All rights reserved.
//

#import "ImageCropViewController.h"
#import "CropperView.h"
#import "ISMessages.h"
#import "CroppedImageArray.h"
#import "GlobalVars.h"
#import "G8ViewController.h"

@interface ImageCropViewController ()
//@property (strong, nonatomic) CropperView * cropperView;
@property (strong, nonatomic) NSArray * cropedViews;
@property (strong, nonatomic) NSData * cropImage;

@end

@implementation ImageCropViewController
@synthesize receiveImage;
@synthesize cropperSourceView;
@synthesize okBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    cropperSourceView = [[CropperView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height)];
    [cropperSourceView setImage:[UIImage imageWithData:receiveImage]];
    [cropperSourceView addCropper:CGRectMake(120, 40, 100, 100)];
    //[cropperSourceView addCropper:CGRectMake(180, 160, 100, 100)];
    
    [self.view addSubview:cropperSourceView];
    
    
    okBtn.layer.cornerRadius = 3;
    //[self.cropperSourceView addSubview:okBtn];
    
    [self showAlert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)okBtnPressed:(id)sender {
    
    //_cropedViews = [cropperSourceView crop];
    _cropImage = UIImagePNGRepresentation([cropperSourceView cropAtIndex:0]);
    
    GlobalVars *gimage = [GlobalVars sharedInstance];
    //gimage.imageList = _cropedViews;
    gimage.cropImage = _cropImage;
    
    NSLog(@"Croped Views Count : %lu",(unsigned long)[gimage.imageList count]);
    
    if (_cropImage != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *documentDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat:@"recogimage.png"];
        NSString *path = [documentDirectory stringByAppendingPathComponent:filename];
        
        NSData* data = _cropImage;
        [data writeToFile:path atomically:YES];
        [GlobalVars sharedInstance].fileName = filename;
        NSLog(@"Save Recog Image: %@",path);
        
        NSDictionary *userInfo = @{ @"message": @"photo_ocr" };
        [[NSNotificationCenter defaultCenter] postNotificationName:@"photo-recognition"
                                                            object:nil
                                                          userInfo:userInfo];
    }
    else
    {
        NSLog(@"Image Save Error!");
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlert
{
    [ISMessages showCardAlertWithTitle:@"BLUWISE-OCR"
                               message:@"데이터 인식을 위해 정확한 영역을 선택 해 주세요.\n이 알림은 터치하면 사라집니다."
                              duration:20.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeInfo
                         alertPosition:0
                               didHide:^(BOOL finished) {
                                   //NSLog(@"Alert did hide.");
                               }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
