//
//  G8ViewController.m
//  Template Framework Project
//
//  Created by Daniele on 14/10/13.
//  Copyright (c) 2013 Daniele Galiotto - www.g8production.com.
//  All rights reserved.
//

#import "G8ViewController.h"
#import "ImageCropViewController.h"
#import "CroppedImageArray.h"
#import "GlobalVars.h"
#import "PhotoTweaksViewController.h"
#import "PhotoTweakView.h"
#import "ISMessages.h"
//#import <opencv2/opencv.hpp>

@interface G8ViewController ()

@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (strong, nonatomic) IBOutlet UIButton *takePhotoBtn;
@property (strong, nonatomic) IBOutlet UIButton *useSampleBtn;
@property (strong, nonatomic) IBOutlet UIButton *clearChacheBtn;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) NSString *language;
//@property (strong, nonatomic) UIImage *transImage;
@property (strong, nonatomic) NSData *transImage;
@end


/**
 *  For more information about using `G8Tesseract`, visit the GitHub page at:
 *  https://github.com/gali8/Tesseract-OCR-iOS
 */
@implementation G8ViewController
//@synthesize imageToRecognize;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create a queue to perform recognition operations
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.activityIndicator.hidden = YES;
    
    _takePhotoBtn.layer.cornerRadius = 3;
    _useSampleBtn.layer.cornerRadius = 3;
    _clearChacheBtn.layer.cornerRadius = 3;
    
    self.logoImageView.layer.cornerRadius = 30;
    self.logoImageView.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveSimpleNotification:)
                                                 name:@"photo-recognition"
                                               object:nil];
     
}

- (void)viewWillAppear:(BOOL)animated
{
    /*
    GlobalVars *gimage = [GlobalVars sharedInstance];
    NSData *image1 = gimage.imageList[0];
    if (gimage.emptyArray == NO) {
        [self recognizeImageWithTesseract:[UIImage imageWithData:image1]];
    }
    */
}

-(void)recognizeImageWithTesseract:(UIImage *)image
{
    // Animate a progress activity indicator
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];

    // Create a new `G8RecognitionOperation` to perform the OCR asynchronously
    // It is assumed that there is a .traineddata file for the language pack
    // you want Tesseract to use in the "tessdata" folder in the root of the
    // project AND that the "tessdata" folder is a referenced folder and NOT
    // a symbolic group in your project
    NSLog(@"_language : %@",_language);
    G8RecognitionOperation *operation = [[G8RecognitionOperation alloc] initWithLanguage:_language];

    // Use the original Tesseract engine mode in performing the recognition
    // (see G8Constants.h) for other engine mode options
    if ([_language isEqualToString:@"eng"]) {
        
        operation.tesseract.engineMode = G8OCREngineModeTesseractCubeCombined;
    }
    
    else if ([_language isEqualToString:@"kor"]) {
        
        operation.tesseract.engineMode = G8OCREngineModeTesseractOnly;  //한국어는 cube file 미지원
    }
    
    else if ([_language isEqualToString:@"eng+kor"]) {
        
        operation.tesseract.engineMode = G8OCREngineModeTesseractOnly;
    }
    
    else if ([_language isEqualToString:@"chi_sim"]) {
        
        operation.tesseract.engineMode = G8OCREngineModeTesseractOnly;
    }
    
    
    // Let Tesseract automatically segment the page into blocks of text
    // based on its analysis (see G8Constants.h) for other page segmentation
    // mode options
    operation.tesseract.pageSegmentationMode = G8PageSegmentationModeAutoOnly;
    
    // Optionally limit the time Tesseract should spend performing the
    // recognition
    //operation.tesseract.maximumRecognitionTime = 1.0;
    
    // Set the delegate for the recognition to be this class
    // (see `progressImageRecognitionForTesseract` and
    // `shouldCancelImageRecognitionForTesseract` methods below)
    operation.delegate = self;

    // Optionally limit Tesseract's recognition to the following whitelist
    // and blacklist of characters
    //operation.tesseract.charWhitelist = @"01234";
    //operation.tesseract.charBlacklist = @"56789";
    
    // Set the image on which Tesseract should perform recognition
    operation.tesseract.image = image;

    // Optionally limit the region in the image on which Tesseract should
    // perform recognition to a rectangle
    //operation.tesseract.rect = CGRectMake(20, 20, 100, 100);

    // Specify the function block that should be executed when Tesseract
    // finishes performing recognition on the image
    operation.recognitionCompleteBlock = ^(G8Tesseract *tesseract) {
        // Fetch the recognized text
        NSString *recognizedText = tesseract.recognizedText;

        NSLog(@"%@", recognizedText);

        // Remove the animated progress activity indicator
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden = YES;

        // Spawn an alert with the recognized text
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"인식 결과"
                                                        message:recognizedText
                                                       delegate:nil
                                              cancelButtonTitle:@"확인"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    // Display the image to be recognized in the view
    self.imageToRecognize.image = operation.tesseract.thresholdedImage;

    // Finally, add the recognition operation to the queue
    [self.operationQueue addOperation:operation];
}

/**
 *  This function is part of Tesseract's delegate. It will be called
 *  periodically as the recognition happens so you can observe the progress.
 *
 *  @param tesseract The `G8Tesseract` object performing the recognition.
 */
- (void)progressImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    NSLog(@"progress: %lu", (unsigned long)tesseract.progress);
}

/**
 *  This function is part of Tesseract's delegate. It will be called
 *  periodically as the recognition happens so you can cancel the recogntion
 *  prematurely if necessary.
 *
 *  @param tesseract The `G8Tesseract` object performing the recognition.
 *
 *  @return Whether or not to cancel the recognition.
 */
- (BOOL)shouldCancelImageRecognitionForTesseract:(G8Tesseract *)tesseract {
    return NO;  // return YES, if you need to cancel recognition prematurely
}

- (IBAction)openCamera:(id)sender
{
    self.language = @"eng+kor+chi_sim";
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    imgPicker.allowsEditing = YES;
    imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:imgPicker animated:YES completion:NULL];
}

- (IBAction)actionSheetButtonPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OCR Example" message:@"샘플 이미지 언어를 선택하세요." preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"영어" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"Select English");
        self.language = @"eng";
        [self recognizeImageWithTesseract:[UIImage imageNamed:@"english_sample.jpg"]];
    }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"한국어" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"Select Korean");
        self.language = @"kor";
        [self recognizeImageWithTesseract:[UIImage imageNamed:@"korean_sample.gif"]];
    }];
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"한국어 처방전 샘플" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"Select Korean");
        self.language = @"kor";
        [self recognizeImageWithTesseract:[UIImage imageNamed:@"hospital.jpg"]];
    }];
    
    UIAlertAction *fourthAction = [UIAlertAction actionWithTitle:@"중국어" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSLog(@"Select Chinese");
        self.language = @"chi_sim";
        [self recognizeImageWithTesseract:[UIImage imageNamed:@"maxresdefault.jpg"]];
    }];
    
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdAction];
    [alert addAction:fourthAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
- (IBAction)recognizeSampleImage:(id)sender {
    [self recognizeImageWithTesseract:[UIImage imageNamed:@"korean_sample.gif"]];
    //[self recognizeImageWithTesseract:[UIImage imageNamed:@"english_sample.jpg"]];
}
*/

- (IBAction)clearCache:(id)sender
{
    [G8Tesseract clearCache];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OCR Sample"
                                                    message:@"캐쉬 데이터가 초기화 되었습니다."
                                                   delegate:nil
                                          cancelButtonTitle:@"확인"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
/*
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    _transImage = UIImagePNGRepresentation(image);

    [self performSegueWithIdentifier:@"imageCropSegue" sender:self];
*/
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:image];
    photoTweaksViewController.delegate = self;
    photoTweaksViewController.autoSaveToLibray = NO;
    photoTweaksViewController.maxRotationAngle = M_PI_2;
    
    [self showAlert];
    [picker pushViewController:photoTweaksViewController animated:YES];
}


#pragma mark - Call img Cropper

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage
{
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self recognizeImageWithTesseract:croppedImage];
}

/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"imageCropSegue"]) {
        ImageCropViewController *destination = [segue destinationViewController];
        destination.receiveImage = _transImage;
    }
}
*/
- (void)showAlert
{
    [ISMessages showCardAlertWithTitle:@"BLUWISE-OCR"
                               message:@"데이터 인식을 위해 정확한 영역을 선택 해 주세요.\n이 알림은 터치하면 사라집니다."
                              duration:20.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeInfo
                         alertPosition:0
                               didHide:^(BOOL finished) {
                                   //NSLog(@"Alert did hide.");
                               }];
}


#pragma mark - UIImage Rotate

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    //Calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    //Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    //Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    //Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Observer did received message

- (void)didReceiveSimpleNotification:(NSNotification *)notification
{
    NSString *message = [notification.userInfo objectForKey:@"message"];
    NSLog(@"I've got the message %@", message);
    
    if ([message isEqualToString:@"photo_ocr"]) {
        NSLog(@"OCR Engine Start..");
        
        NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *filePath = [documentPath stringByAppendingPathComponent:[GlobalVars sharedInstance].fileName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        
        if (fileExists) {
            NSLog(@"Img is Exist.");
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *filename = [GlobalVars sharedInstance].fileName;
            NSString* path = [documentsDirectory stringByAppendingPathComponent:
                              filename];
            UIImage* image = [UIImage imageWithContentsOfFile:path];
            [self recognizeImageWithTesseract:image];
        }
        else
        {
            NSLog(@"Img is Not Exist.");
        }
        
        /*
        GlobalVars *gimage = [GlobalVars sharedInstance];
        //UIImage *image1 = gimage.imageList[0];
        UIImage *image1 = [UIImage imageWithData:gimage.cropImage];
        [self recognizeImageWithTesseract:image1];
         */
    }
}
@end
